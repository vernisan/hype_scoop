class TwitterService
  
  def findUserByName (twitterUserName)
    user = TwitterRestClient.user(twitterUserName)   
    
    return convertToModel(user)
  end
  
  def searchRecentUserRelatedTweets (twitterUser, count = 20)
    twitterUserScreenName = twitterUser.screen_name
    
    query = "@#{twitterUserScreenName} OR to:#{twitterUserScreenName} OR (\##{twitterUserScreenName} -rt)"
    tweets = TwitterRestClient.search(query, lang: "en", result_type: "recent", count: count)
    
    if not tweets.nil?
      return tweets.inject([]) do |acc, foundTweet|
        tweet = Tweet.new
        
        tweet.id = foundTweet.id
        tweet.text = foundTweet.text
        tweet.user = convertToModel(foundTweet.user)
        tweet.created_at = foundTweet.created_at
        
        acc.push(tweet)
      end
    else
      return []
    end
  end
  
  private
  
  def convertToModel(user)
    twitterUser = TwitterUser.new
    
    twitterUser.id = user.id
    twitterUser.name = user.name
    twitterUser.profile_image_url_https = user.profile_image_url_https
    twitterUser.screen_name = user.screen_name
    
    return twitterUser
  end
  
end