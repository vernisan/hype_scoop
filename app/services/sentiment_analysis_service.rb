class SentimentAnalysisService
  
  def analyzeTweetsSentiment (tweets)
    sentimentAnalysis = SentimentAnalysis.new
      
    moodMap = {}
    averageScore = nil
    averageSentiment = nil
    
    if tweets.blank? == false
      totalScore = 0.0
      totalSentiment = 0
      
      tweets.map do |tweet|
        moodMap[tweet.id] = {}
        
        sentiment = SentimentalAnalyzer.sentiment tweet.text
        score = SentimentalAnalyzer.score tweet.text
        
        moodMap[tweet.id][:sentiment] = sentiment
        moodMap[tweet.id][:score] = score
        
        totalScore = totalScore + score.to_f
        
        case sentiment
        when :positive
          totalSentiment = totalSentiment + 2
        when :neutral
          totalSentiment = totalSentiment + 1
        end
      end
      
      totalTweets = tweets.size
      
      averageScore = totalScore / totalTweets
      averageSentiment = totalSentiment.to_f / totalTweets.to_f
       
      threshold = SentimentalAnalyzer.threshold.to_f
      
      if averageScore > threshold
        averageSentiment = :positive
      elsif averageScore >= -threshold
        averageSentiment = :neutral
      else
        averageSentiment = :negative
      end
    end
    
    sentimentAnalysis.moodMap = moodMap
    sentimentAnalysis.averageScore = averageScore
    sentimentAnalysis.averageSentiment = averageSentiment
    
    return sentimentAnalysis
  end
  
  
  
end