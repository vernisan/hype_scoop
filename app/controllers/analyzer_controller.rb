class AnalyzerController < ApplicationController
  
  def analyze
    @twitterUserName = params[:user]
    
    if @twitterUserName.blank?
      flash.now[:danger] = "The user name must no be empty"
    else
      begin 
        @twitterUser = findUserByName(@twitterUserName)
        @tweets = searchLast20UserRelatedTweets(@twitterUser)
        analyzeTweetsSentiment(@tweets)
      rescue Twitter::Error::NotFound
        flash.now[:danger] = "Sorry, this user was not found"
      rescue => e
        flash.now[:danger] = "Sorry, we had a problem retrieving this user tweets. Try again later, please."
        @apiError = true
        logger.error e.message
        e.backtrace.each { |line| logger.error line }
      end
    end
  end
  
  private
  
  def findUserByName(twitterUserName)
    return TwitterService.new.findUserByName(@twitterUserName)
  end
  
  def searchLast20UserRelatedTweets(twitterUser)
    return TwitterService.new.searchRecentUserRelatedTweets(twitterUser).take(20)
  end
  
  def analyzeTweetsSentiment (tweets)
    if tweets.nil? == false
      sentimentAnalysis = SentimentAnalysisService.new.analyzeTweetsSentiment(tweets)    
      
      @moodMap = sentimentAnalysis.moodMap
      @averageScore = sentimentAnalysis.averageScore
      @averageSentiment = sentimentAnalysis.averageSentiment
    end
  end
  
end
