Rails.application.routes.draw do
  
  root 'main_controller#index'
  get  '/about',    to: 'static_pages#about'
  get  '/analyze',    to: 'analyzer#analyze'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
