# HypeScoop

Simple Twitter sentiment analyzer app, written in Rails. https://hype-scoop.herokuapp.com/

## How it works

Type the name of a twitter user, and HypeScoop will fetch the last 20 tweets related to this user, analyze their sentiment, and assign a HypeMeter to the user.

The HypeMeter is either positive, neutral or negative, based on the average sentiment of the tweets. 

The app uses the sentimental gem to categorize the tweets. More info in: https://github.com/7compass/sentimental

The tweets must be recent (Twitter API doesn't allow fetching old ones), and written in english, no other language is accepted. 


## Getting started

First, you need to configure your twitter credentials. Create a file named config/application.yml and place your keys there, like this:

```
TWITTER_CONSUMER_KEY: "REPLACE_WITH_YOUR_TWITTER_KEY"
TWITTER_CONSUMER_SECRET: "REPLACE_WITH_YOUR_TWITTER_SECRET"
```

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install
```

The app does not use a database, there is no need to run rails db:migrate. You can just run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server