require 'test_helper'

class TwitterServiceTest < ActiveSupport::TestCase
  
  test 'should find a valid user' do
     twitterService = TwitterService.new
     
     foundUser = stub(:id=> 1234, :name => "JohnDoe", :screen_name => "JohnDoe", :profile_image_url_https => "http://www.example.com/image.png")
     
     TwitterRestClient.expects(:user).with("JohnDoe").returns(foundUser)
     
     twitterUser = twitterService.findUserByName("JohnDoe")
     
     assert_equal 1234, twitterUser.id
     assert_equal "JohnDoe", twitterUser.name
     assert_equal "JohnDoe", twitterUser.screen_name
  end
  
  test 'should return recent user tweets' do
     twitterService = TwitterService.new
      
     user1 = stub(:id=> 1234, :name => "JohnDoe", :screen_name => "JohnDoe", :profile_image_url_https => "http://www.example.com/image.png")
     user2 = stub(:id=> 4321, :name => "JohnDoeJr", :screen_name => "JohnDoeJr", :profile_image_url_https => "http://www.example.com/image2.png")
      
     foundTweets = []
     foundTweets << stub(:id => 1, :text => "some nice tweet", :created_at => "2016-12-20 16:10:18 +000", :user => user1) 
     foundTweets << stub(:id => 2, :text => "my sweet tweet", :created_at => "2016-12-20 16:20:18 +000", :user => user2) 
      
     TwitterRestClient.expects(:search).returns(foundTweets) 
      
     twitterUser = TwitterUser.new 
     twitterUser.screen_name = "everlane" 
      
     tweets = twitterService.searchRecentUserRelatedTweets(twitterUser)
     
     assert_equal 2, tweets.size
     assert validateTweet(1, "some nice tweet", "2016-12-20 16:10:18 +000", user1, tweets[0])
     assert validateTweet(2, "my sweet tweet", "2016-12-20 16:20:18 +000", user2, tweets[1])
  end
  
  def validateTweet(id, text, created_at, user, tweet)
     assert_equal id, tweet.id
     assert_equal text, tweet.text
     assert_equal user.id, tweet.user.id
     assert_equal created_at, tweet.created_at
  end
  
end
