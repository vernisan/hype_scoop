require 'test_helper'

class SentimentAnalysisServiceTest < ActiveSupport::TestCase
  
  test 'should generate empty analysis when no tweets' do
    sentimentAnalysisService = SentimentAnalysisService.new
    
    tweets = []
    
    sentimentAnalysis = sentimentAnalysisService.analyzeTweetsSentiment(tweets)
    
    moodMap = sentimentAnalysis.moodMap
    
    assert_equal 0, moodMap.size
    assert_nil sentimentAnalysis.averageScore
    assert_nil sentimentAnalysis.averageSentiment
  end
  
  test 'should categorize positive tweets into positive' do
    sentimentAnalysisService = SentimentAnalysisService.new

    SentimentalAnalyzer.expects(:threshold).returns(0.1)
    SentimentalAnalyzer.expects(:sentiment).with("love is so lovely").returns(:positive)
    SentimentalAnalyzer.expects(:score).with("love is so lovely").returns(0.50)
    SentimentalAnalyzer.expects(:sentiment).with("love is the air").returns(:positive)
    SentimentalAnalyzer.expects(:score).with("love is the air").returns(0.40)  
    
    tweet1 = Tweet.new
    tweet1.id = 1
    tweet1.text = "love is so lovely"
    
    tweet2 = Tweet.new
    tweet2.id = 2
    tweet2.text = "love is the air"
    
    tweets = [tweet1, tweet2]
    
    sentimentAnalysis = sentimentAnalysisService.analyzeTweetsSentiment(tweets)
    
    moodMap = sentimentAnalysis.moodMap
    
    assert_equal :positive, moodMap[1][:sentiment]
    assert_equal 0.5, moodMap[1][:score]
    assert_equal :positive, moodMap[2][:sentiment]
    assert_equal 0.4, moodMap[2][:score]
    
    assert_equal 0.45, sentimentAnalysis.averageScore
    assert_equal :positive, sentimentAnalysis.averageSentiment
  end
  
  test 'should categorize mostly positive tweets into positive' do
    sentimentAnalysisService = SentimentAnalysisService.new

    SentimentalAnalyzer.expects(:threshold).returns(0.1)
    SentimentalAnalyzer.expects(:sentiment).with("love is so lovely").returns(:positive)
    SentimentalAnalyzer.expects(:score).with("love is so lovely").returns(0.50)
    SentimentalAnalyzer.expects(:sentiment).with("love is the air").returns(:positive)
    SentimentalAnalyzer.expects(:score).with("love is the air").returns(0.40)  
    SentimentalAnalyzer.expects(:sentiment).with("hate is dangerous").returns(:positive)
    SentimentalAnalyzer.expects(:score).with("hate is dangerous").returns(-0.30)  
    
    tweet1 = Tweet.new
    tweet1.id = 1
    tweet1.text = "love is so lovely"
    
    tweet2 = Tweet.new
    tweet2.id = 2
    tweet2.text = "love is the air"
    
    tweet3 = Tweet.new
    tweet3.id = 3
    tweet3.text = "hate is dangerous"
    
    tweets = [tweet1, tweet2, tweet3]
    
    sentimentAnalysis = sentimentAnalysisService.analyzeTweetsSentiment(tweets)
    
    moodMap = sentimentAnalysis.moodMap
    
    assert_equal :positive, moodMap[1][:sentiment]
    assert_equal 0.5, moodMap[1][:score]
    assert_equal :positive, moodMap[2][:sentiment]
    assert_equal 0.4, moodMap[2][:score]
    assert_equal :positive, moodMap[3][:sentiment]
    assert_equal (-0.3), moodMap[3][:score]
    
    assert_equal 0.2, sentimentAnalysis.averageScore.round(2)
    assert_equal :positive, sentimentAnalysis.averageSentiment
  end
  
end