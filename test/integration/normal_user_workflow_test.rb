require 'test_helper'

class NormalUserWorkflowTest < ActionDispatch::IntegrationTest
  
  test "should be able to search for user and get a full sentiment analysis" do
    get root_path
    assert_response :success
    assert_template 'main_controller/index'
    assert_select "title", "HypeScoop"
    
    get analyze_path, params: {:user => "everlane"}
    assert_response :success
    
    assert_template 'analyzer/analyze'
    assert_select "title", "HypeScoop"
    assert_select "div#hype-meter", true, "The rendered page must contain a HypeMeter"
    assert_select "div#hype-score", true, "The rendered page must contain a HypeScore"
    assert_select "div.tweet-row", true, "The rendered page must contain tweets"
    assert_select "div.alert", false, "The rendered page must contain no flash alerts"
  end
  
end
