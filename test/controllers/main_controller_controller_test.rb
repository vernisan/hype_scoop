require 'test_helper'

class MainControllerControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "HypeScoop"
  end

end
