require 'test_helper'

class AnalyzerControllerTest < ActionDispatch::IntegrationTest
  
  test "should show flash message when no user specified" do
    get analyze_path
    assert_response :success
    
    assert_template 'analyzer/analyze'
    assert_select "title", "HypeScoop"
    assert_select "div.alert.alert-danger", "The user name must no be empty"
  end
  
  test "should show flash message when user not found" do
    get analyze_path, params: {:user => "SomeUnexistentUser-randomString1212"}
    assert_response :success
    
    assert_template 'analyzer/analyze'
    assert_select "title", "HypeScoop"
    assert_select "div.alert.alert-danger", "Sorry, this user was not found"
  end
  
  test "should show tweets and scores when user is valid" do
    get analyze_path, params: {:user => "everlane"}
    assert_response :success
    
    assert_template 'analyzer/analyze'
    assert_select "title", "HypeScoop"
    assert_select "div.tweet-row", true, "The rendered page must contain tweets"
    assert_select "div.alert", false, "The rendered page must contain no flash alerts"
  end
  
  test 'should show warning message when user has no recent related tweets' do
    get analyze_path, params: {:user => "assinerabixo"}
    assert_response :success
    
    assert_template 'analyzer/analyze'
    assert_select "title", "HypeScoop"
    assert_select "div#no-tweets-warn", true, "The rendered page must contain a no tweets warning message"
  end

end
